/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solemne3apii.servicios;

import com.mycompany.entity.Direcciones;
import com.mycompany.solemne3apii.dao.DireccionesJpaController;
import com.mycompany.solemne3apii.dao.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author GorillaSetups
 */
@Path("direcciones")
public class DireccionesRest {
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarDirecciones(){
        DireccionesJpaController dao=new DireccionesJpaController();
        List<Direcciones> direcciones=dao.findDireccionesEntities();
        return Response.ok(200).entity(direcciones).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
   public Response crear(Direcciones direcciones){
       DireccionesJpaController dao=new DireccionesJpaController();
       
        try {
            dao.create(direcciones);
        } catch (Exception ex) {
            Logger.getLogger(DireccionesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return Response.ok(200).entity(direcciones).build();
   } 
    
   @PUT
   @Produces(MediaType.APPLICATION_JSON)
   public Response actualizar(Direcciones direcciones){
       DireccionesJpaController dao=new DireccionesJpaController();
        try {
            dao.edit(direcciones);
        } catch (Exception ex) {
            Logger.getLogger(DireccionesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok(200).entity(direcciones).build();
   }
   
   @DELETE
   @Path("/{ideliminar}")
   @Produces(MediaType.APPLICATION_JSON)        
   public Response elminar(@PathParam("ideliminar") String ideliminar){
       DireccionesJpaController dao=new DireccionesJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(DireccionesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok("direccion eliminada").build();
   }
   
   @GET
   @Path("/{idConsultar}")
   @Produces(MediaType.APPLICATION_JSON)
   public Response consultarPorRut(@PathParam("idConsultar") String idConsultar){
      DireccionesJpaController dao=new DireccionesJpaController();
      Direcciones direcciones= dao.findDirecciones(idConsultar);
      return Response.ok(200).entity(direcciones).build();
   }
    
}


